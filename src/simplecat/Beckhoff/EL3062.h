/**
 * This file is part of SimpleECAT.
 *
 * SimpleECAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimplECAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with SimpleECAT.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \class Beckhoff_EL3062
 *
 * \ingroup SimplECAT
 *
 * \brief Beckhoff EL3062
 *
 * Two analog inputs +/- 10V
 */


#ifndef SIMPLECAT_BECKHOFF_EL3062_H_
#define SIMPLECAT_BECKHOFF_EL3062_H_

#include <simplecat/Slave.h>

namespace simplecat {


class Beckhoff_EL3062 : public Slave
{

public:
    Beckhoff_EL3062() : Slave(0x00000002, 0x0bf63052) {}
    virtual ~Beckhoff_EL3062() {}

    virtual void processData(size_t index, uint8_t* domain_address){
        read_data_[index] = EC_READ_S16(domain_address);
    }

    virtual const ec_sync_info_t* syncs() { return &syncs_[0]; }

    virtual size_t syncSize() {
        return sizeof(syncs_)/sizeof(ec_sync_info_t);
    }

    virtual const ec_pdo_entry_info_t* channels() {
        return channels_;
    }

    virtual void domains(DomainMap& domains) const {
        domains = domains_;
    }

 	//array to store the data to be read or sent
    int16_t read_data_[2] = {0}; //example

private:
    ec_pdo_entry_info_t channels_[20] = {
        {0x6000, 0x01, 1},
        {0x6000, 0x02, 1},
        {0x6000, 0x03, 2},
        {0x6000, 0x05, 2},
        {0x6000, 0x07, 1},
        {0x0000, 0x00, 1}, /* Gap */
        {0x0000, 0x00, 6}, /* Gap */
        {0x6000, 0x0f, 1},
        {0x6000, 0x10, 1},
        {0x6000, 0x11, 16},
        {0x6010, 0x01, 1},
        {0x6010, 0x02, 1},
        {0x6010, 0x03, 2},
        {0x6010, 0x05, 2},
        {0x6010, 0x07, 1},
        {0x0000, 0x00, 1}, /* Gap */
        {0x0000, 0x00, 6}, /* Gap */
        {0x6010, 0x0f, 1},
        {0x6010, 0x10, 1},
        {0x6010, 0x11, 16},
    };

    ec_pdo_info_t pdos_[2] = {
        {0x1a00, 10, channels_ + 0},
        {0x1a02, 10, channels_ + 10},
    };

    ec_sync_info_t syncs_[2] = {
      	{3, EC_DIR_INPUT, 2, pdos_ + 0, EC_WD_DISABLE},
        {0xff}
    };

    DomainMap domains_ = {
        {0, {9,19} }
    };
};

}

#endif
